//
// Created by elenappli on 11/15/16.
//

#ifndef QUBYSERVICE_XMLPARSINGLIBTESTDATA_H_H
#define QUBYSERVICE_XMLPARSINGLIBTESTDATA_H_H

#include <gtest/gtest.h>

#include <limits.h>

class xmlParsingLibTestData {
public:
    xmlParsingLibTestData() {
        mBaseDir = "/home/elenappli/ClionProjects/QubyService/";
        mPath = "tests/xmlParsingLib_tests/testXmls/";
    }

    std::string mBaseDir;
    std::string mPath;
};

xmlParsingLibTestData gXmlParsingLibTestData;

#endif //QUBYSERVICE_XMLPARSINGLIBTESTDATA_H_H
