//
// Created by elenappli on 11/12/16.
//

extern "C" {
#include <xmlParsingLib/msgTypeParser.h>
#include <testUtils/messageBuilder.h>
}

#include "xmlParsingLibTestData.h"

#include <string>

#include <gtest/gtest.h>


class msgTypeParser_test : public ::testing::Test {
protected:
    virtual void SetUp() {
    }

    virtual void TearDown() {
        // Code here will be called immediately after each test
        // (right before the destructor).
    }
};

TEST_F(msgTypeParser_test, msgTypeParser_TEST_ItIsAnUpdateMsg) {
    msgType expType = UPDATE;
    char *msg;
    const std::string fileName =
            gXmlParsingLibTestData.mBaseDir +
            gXmlParsingLibTestData.mPath +
            "update1.xml";
    msg = fileToString(fileName.c_str());
    msgType actType = getMsgType(msg);

    EXPECT_EQ(expType, actType);
}

TEST_F(msgTypeParser_test, msgTypeParser_TEST_ItIsARetrieveAllMsg) {
    msgType expType = RETRIEVE_ALL;
    char *msg;
    const std::string fileName =
            gXmlParsingLibTestData.mBaseDir +
            gXmlParsingLibTestData.mPath +
            "retrieveAll.xml";
    msg = fileToString(fileName.c_str());
    msgType actType = getMsgType(msg);

    EXPECT_EQ(expType, actType);
}

TEST_F(msgTypeParser_test, msgTypeParser_TEST_ItIsARetrieveSubMsg) {
    msgType expType = RETRIEVE_SUB;
    char *msg;
    const std::string fileName =
            gXmlParsingLibTestData.mBaseDir +
            gXmlParsingLibTestData
                    .mPath +
            "retrieve1.xml";
    msg = fileToString(fileName.c_str());
    msgType actType = getMsgType(msg);

    EXPECT_EQ(expType, actType);
}

TEST_F(msgTypeParser_test, msgTypeParser_TEST_ItIsAnInvalidMsg) {
    msgType expType = INVALID;
    char *msg;
    const std::string fileName =
            gXmlParsingLibTestData.mBaseDir +
            gXmlParsingLibTestData.mPath +
            "invalidMsg.xml";
    msg = fileToString(fileName.c_str());
    msgType actType = getMsgType(msg);

    EXPECT_EQ(expType, actType);
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
