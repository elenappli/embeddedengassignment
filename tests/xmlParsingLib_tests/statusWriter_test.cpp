//
// Created by elenappli on 11/14/16.
//

extern "C" {
#include <xmlParsingLib/updateReader.h>
#include <xmlParsingLib/statusWriter.h>
#include <testUtils/messageBuilder.h>
}

#include "xmlParsingLibTestData.h"

#include <gtest/gtest.h>

class statusWriter_test : public ::testing::Test {
protected:
    virtual void SetUp() {
    }

    virtual void TearDown() {
        // Code here will be called immediately after each test
        // (right before the destructor).
    }
};

TEST_F(statusWriter_test, statusWriter_TEST_initialStatusXmlEmpty) {
    ezxml_t status;
    status = createNewStatusXml();
    const std::string fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "statusEmpty.xml";

    char *expStatus;
    expStatus = fileToString(fname.c_str());

    char *strStatus;
    strStatus = ezxml_toxml(status);

    EXPECT_EQ(strlen(expStatus), strlen(strStatus));
    for (int ii = 0; ii < strlen(strStatus); ii++) {
        EXPECT_EQ(expStatus[ii], strStatus[ii]);
    }

    free(expStatus);
    free(strStatus);
    ezxml_free(status);
}

TEST_F(statusWriter_test, statusWriter_TEST_WritingStatusXmlToFile) {
    char *msg;
    const std::string msgFname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "update1.xml";
    const std::string expFname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "statusA.xml";
    const std::string statusFname = "/home/elenappli/.CLion12/system/cmake/generated/f5936702/f5936702/Debug/tests/xmlParsingLib_tests/status.xml";

    msg = fileToString(msgFname.c_str());
    //init
    ezxml_t status;
    status = createNewStatusXml();

    //read update 1
    size_t len = strlen(msg);
    readUpdate(msg, len, &status);

    writeStatusToFile(&status);

    char *expfile;
    expfile = fileToString(expFname.c_str());

    char *statusfile;
    statusfile = fileToString(statusFname.c_str());//ezxml_toxml(status);

    EXPECT_EQ(strlen(expfile), strlen(statusfile));
    for (int ii = 0; ii < strlen(statusfile); ii++) {
        EXPECT_EQ(expfile[ii], statusfile[ii]);
    }

    free(msg);
    free(expfile);
    ezxml_free(status);
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
