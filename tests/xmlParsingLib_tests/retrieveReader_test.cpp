//
// Created by elenappli on 11/12/16.
//

extern "C" {
#include <xmlParsingLib/retrieveReader.h>
#include <xmlParsingLib/updateReader.h>
#include <xmlParsingLib/statusWriter.h>
#include <testUtils/messageBuilder.h>
}

#include "xmlParsingLibTestData.h"

#include <gtest/gtest.h>

class retrieveReader_test : public ::testing::Test {
protected:
    virtual void SetUp() {
    }

    virtual void TearDown() {
        // Code here will be called immediately after each test
        // (right before the destructor).
    }
};

TEST_F(retrieveReader_test, retrieveReader_TEST_ReadingASeriesOfRetrieveReqests) {
    char *msgUpdate1, *msgUpdate2, *msgUpdate3;
    char *msgRetrieve1, *msgRetrieve2, *msgRetrieve3, *msgRetrieveAll;
    char *status1, *status2, *status3, *status4;
    ezxml_t msg1, msg2, msg3;
    char *rsp;
    const std::string update1Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "update1.xml";
    const std::string update2Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "update2.xml";
    const std::string update3Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "update3.xml";
    const std::string retrieve1Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "retrieve1.xml";
    const std::string retrieve2Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "retrieve2.xml";
    const std::string retrieve3Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "retrieve3.xml";
    const std::string retrieveAllFname =
            gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "retrieveAll.xml";
    const std::string status1Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "status1.xml";
    const std::string status2Fname =
            gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "statusRetrieve2.xml";
    const std::string status3Fname =
            gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "statusRetrieve3.xml";
    const std::string status4Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "status3.xml";

    msgUpdate1 = fileToString(update1Fname.c_str());
    msgUpdate2 = fileToString(update2Fname.c_str());
    msgUpdate3 = fileToString(update3Fname.c_str());
    msgRetrieve1 = fileToString(retrieve1Fname.c_str());
    msgRetrieve2 = fileToString(retrieve2Fname.c_str());
    msgRetrieve3 = fileToString(retrieve3Fname.c_str());
    msgRetrieveAll = fileToString(retrieveAllFname.c_str());
    status1 = fileToString(status1Fname.c_str());
    status2 = fileToString(status2Fname.c_str());
    status3 = fileToString(status3Fname.c_str());
    status4 = fileToString(status4Fname.c_str());

    //init, status empty
    ezxml_t status;
    status = createNewStatusXml();

    //update1 sent
    size_t len = strlen(msgUpdate1);
    readUpdate(msgUpdate1, len, &status);

    //send retrieve1 msg
    len = strlen((msgRetrieve1));
    msg1 = readRetrieveRequest(msgRetrieve1, len, &status);
    rsp = ezxml_toxml(msg1);
    //check status is equal to status1.xml
    EXPECT_EQ(strlen(status1), strlen(rsp));
    for (int ii = 0; ii < strlen(rsp); ii++) {
        EXPECT_EQ(status1[ii], rsp[ii]);
    }
    free(rsp);

    //update 2 sent
    len = strlen(msgUpdate2);
    readUpdate(msgUpdate2, len, &status);
    //update 3 sent
    len = strlen(msgUpdate3);
    readUpdate(msgUpdate3, len, &status);
    //retrieve2 sent
    len = strlen((msgRetrieve2));
    msg2 = readRetrieveRequest(msgRetrieve2, len, &status);

    //check status equal statusRetrieve2
    rsp = ezxml_toxml(msg2);
    EXPECT_EQ(strlen(status2), strlen(rsp));
    for (int ii = 0; ii < strlen(rsp); ii++) {
        EXPECT_EQ(status2[ii], rsp[ii]);
    }
    free(rsp);

    //retrieve3 sent
    len = strlen((msgRetrieve3));
    msg3 = readRetrieveRequest(msgRetrieve3, len, &status);
    //check status equal statusRetrieve3
    rsp = ezxml_toxml(msg3);
    EXPECT_EQ(strlen(status3), strlen(rsp));
    for (int ii = 0; ii < strlen(rsp); ii++) {
        EXPECT_EQ(status3[ii], rsp[ii]);
    }
    free(rsp);

    //retrieve all sent
    len = strlen((msgRetrieveAll));
    rsp = ezxml_toxml(status);
    //check status equal to status3
    EXPECT_EQ(strlen(status4), strlen(rsp));
    for (int ii = 0; ii < strlen(rsp); ii++) {
        EXPECT_EQ(status4[ii], rsp[ii]);
    }
    free(rsp);
    free(msgUpdate1);
    free(msgUpdate2);
    free(msgUpdate3);
    free(msgRetrieve1);
    free(msgRetrieve2);
    free(msgRetrieve3);
    free(msgRetrieveAll);
    ezxml_free(msg1);
    ezxml_free(msg2);
    ezxml_free(msg3);
    ezxml_free(status);
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
