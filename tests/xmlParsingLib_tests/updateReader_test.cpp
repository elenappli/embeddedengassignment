//
// Created by elenappli on 11/12/16.
//

extern "C" {
#include <xmlParsingLib/updateReader.h>
#include <xmlParsingLib/statusWriter.h>
#include <testUtils/messageBuilder.h>
}

#include "xmlParsingLibTestData.h"

#include <gtest/gtest.h>

class updateReader_test : public ::testing::Test {
protected:
    virtual void SetUp() {
    }

    virtual void TearDown() {
        // Code here will be called immediately after each test
        // (right before the destructor).
    }
};

TEST_F(updateReader_test, updateReader_TEST_ReadASeriesOfThreeUpdates) {
    char *msg1, *msg2, *msg3;
    char *statusStr1, *statusStr2, *statusStr3;
    char *expStatusStr1, *expStatusStr2, *expStatusStr3;

    const std::string msg1Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "update1.xml";
    const std::string msg2Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "update2.xml";
    const std::string msg3Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "update3.xml";

    const std::string status1Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "status1.xml";
    const std::string status2Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "status2.xml";
    const std::string status3Fname = gXmlParsingLibTestData.mBaseDir + gXmlParsingLibTestData.mPath + "status3.xml";

    msg1 = fileToString(msg1Fname.c_str());
    msg2 = fileToString(msg2Fname.c_str());
    msg3 = fileToString(msg3Fname.c_str());

    expStatusStr1 = fileToString(status1Fname.c_str());
    expStatusStr2 = fileToString(status2Fname.c_str());
    expStatusStr3 = fileToString(status3Fname.c_str());

    //init
    ezxml_t status;
    status = createNewStatusXml();

    //read update 1
    size_t len = strlen(msg1);
    readUpdate(msg1, len, &status);
    statusStr1 = ezxml_toxml(status);
    //check
    EXPECT_EQ(strlen(expStatusStr1), strlen(statusStr1));
    for (int ii = 0; ii < strlen(statusStr1); ii++) {
        EXPECT_EQ(expStatusStr1[ii], statusStr1[ii]);
    }

    //read update 2
    len = strlen(msg2);
    readUpdate(msg2, len, &status);
    statusStr2 = ezxml_toxml(status);
    //check
    EXPECT_EQ(strlen(expStatusStr2), strlen(statusStr2));
    for (int ii = 0; ii < strlen(statusStr2); ii++) {
        EXPECT_EQ(expStatusStr2[ii], statusStr2[ii]);
    }

    //read update 3
    len = strlen(msg3);
    readUpdate(msg3, len, &status);
    statusStr3 = ezxml_toxml(status);
    //check
    EXPECT_EQ(strlen(expStatusStr3), strlen(statusStr3));
    for (int ii = 0; ii < strlen(statusStr3); ii++) {
        EXPECT_EQ(expStatusStr3[ii], statusStr3[ii]);
    }

    free(msg1);
    free(msg2);
    free(msg3);
    free(statusStr1);
    free(statusStr2);
    free(statusStr3);
    free(expStatusStr1);
    free(expStatusStr2);
    free(expStatusStr3);
    ezxml_free(status);
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
