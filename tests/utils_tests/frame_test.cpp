//
// Created by elenappli on 11/12/16.
//

extern "C" {
#include <utils/frame.h>
#include <testUtils/messageBuilder.h>
}

#include <xmlParsingLibTestData.h>
#include <gtest/gtest.h>
#include <utils/frame.h>

class frame_test : public ::testing::Test {
protected:
    virtual void SetUp() {
    }

    virtual void TearDown() {
        // Code here will be called immediately after each test
        // (right before the destructor).
    }
};

TEST_F(frame_test, frame_TEST_ReadingWholeSmallMsg) {
    frame_t frame;
    frame.index = 0;
    frame.length = 0;
    frame.state = HEADER;
    char *file;
    std::string fname = gXmlParsingLibTestData.mBaseDir +
                        gXmlParsingLibTestData.mPath +
                        "updateX.xml";
    file = fileToString(fname.c_str());
    int len = (int) strlen(file);
    int numBytes = len + 2;
    char *msg;
    msg = (char *) malloc(sizeof(char) * (numBytes));


    msg[0] = 0x00FF & (len >> 8);
    msg[1] = 0x00FF & (len);

    for (int ii = 0; ii < len; ii++) {
        msg[2 + ii] = file[ii];
    }

    int ret = addBytesToFrame(&frame, msg, numBytes);
    EXPECT_EQ(1, ret);
    EXPECT_EQ(len, frame.length);
    for (int ii = 0; ii < frame.length; ii++) {
        EXPECT_EQ(file[ii], frame.payload[ii]);
    }
}

TEST_F(frame_test, frame_TEST_ReadingWholeBigMsg) {
    frame_t frame;
    frame.index = 0;
    frame.length = 0;
    frame.state = HEADER;
    char *file;
    std::string fname = gXmlParsingLibTestData.mBaseDir +
                        gXmlParsingLibTestData.mPath +
                        "update1.xml";
    file = fileToString(fname.c_str());
    int len = (int) strlen(file);
    int numBytes = len + 2;
    char *msg;
    msg = (char *) malloc(sizeof(char) * (numBytes));


    msg[0] = 0x00FF & (len >> 8);
    msg[1] = 0x00FF & (len);

    for (int ii = 0; ii < len; ii++) {
        msg[2 + ii] = file[ii];
    }

    int ret = addBytesToFrame(&frame, msg, numBytes);
    EXPECT_EQ(1, ret);
    EXPECT_EQ(len, frame.length);
    EXPECT_EQ(HEADER, frame.state);
    EXPECT_EQ(0, frame.index);
    for (int ii = 0; ii < frame.length; ii++) {
        EXPECT_EQ(file[ii], frame.payload[ii]);
    }
}

TEST_F(frame_test, frame_TEST_ReadingOneMsgInTwoParts) {
    frame_t frame;
    frame.index = 0;
    frame.length = 0;
    frame.state = HEADER;
    char *file;
    std::string fname = gXmlParsingLibTestData.mBaseDir +
                        gXmlParsingLibTestData.mPath +
                        "updateX.xml";
    file = fileToString(fname.c_str());
    int len = (int) strlen(file);
    char *msgPart1, *msgPart2;
    int numBytesPart1 = 2 + (len >> 1);
    int numBytesPart2 = (len >> 1);
    msgPart1 = (char *) malloc(sizeof(char) * (numBytesPart1));
    msgPart2 = (char *) malloc(sizeof(char) * (numBytesPart2));

    msgPart1[0] = 0x00FF & (len >> 8);
    msgPart1[1] = 0x00FF & (len);

    for (int ii = 0; ii < (len >> 1); ii++) {
        msgPart1[2 + ii] = file[ii];
    }
    for (int ii = 0; ii < numBytesPart2; ii++) {
        msgPart2[ii] = file[(len >> 1) + ii];
    }

    int ret = addBytesToFrame(&frame, msgPart1, numBytesPart1);
    EXPECT_EQ(0, ret);
    EXPECT_EQ(PAYLOAD, frame.state);
    EXPECT_EQ(len, frame.length);

    ret = addBytesToFrame(&frame, msgPart2, numBytesPart2);
    EXPECT_EQ(1, ret);
    for (int ii = 0; ii < frame.length; ii++) {
        EXPECT_EQ(file[ii], frame.payload[ii]);
    }
}

TEST_F(frame_test, frame_TEST_ReadingSecondMsgAfterFirst) {
    frame_t frame;
    frame.index = 0;
    frame.length = 0;
    frame.state = HEADER;
    char *file, *file2;
    std::string fname = gXmlParsingLibTestData.mBaseDir +
                        gXmlParsingLibTestData.mPath +
                        "update1.xml";
    file = fileToString(fname.c_str());
    int len = (int) strlen(file);
    int numBytes = len + 2;
    char *msg;
    msg = (char *) malloc(sizeof(char) * (numBytes));
    msg[0] = 0x00FF & (len >> 8);
    msg[1] = 0x00FF & (len);
    for (int ii = 0; ii < len; ii++) {
        msg[2 + ii] = file[ii];
    }
    int ret = addBytesToFrame(&frame, msg, numBytes);
    EXPECT_EQ(len, frame.length);
    std::string fname2 = gXmlParsingLibTestData.mBaseDir +
                         gXmlParsingLibTestData.mPath +
                         "retrieve1.xml";
    file2 = fileToString(fname2.c_str());
    len = (int) strlen(file2);
    numBytes = len + 2;
    char *msg2;
    msg2 = (char *) malloc(sizeof(char) * numBytes);
    msg2[0] = (char) (0x00FF & (len >> 8));
    msg2[1] = (char) (0x00FF & (len));
    for (int ii = 0; ii < len; ii++) {
        msg2[2 + ii] = file2[ii];
    }
    ret = addBytesToFrame(&frame, msg2, numBytes);
    EXPECT_EQ(1, ret);
    EXPECT_EQ(len, frame.length);
    EXPECT_EQ(HEADER, frame.state);
    EXPECT_EQ(0, frame.index);
    for (int ii = 0; ii < frame.length; ii++) {
        EXPECT_EQ(file2[ii], frame.payload[ii]);
    }

}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
