//
// Created by elenappli on 11/14/16.
//

#ifndef QUBYSERVICE_MESSAGEBUILDER_H
#define QUBYSERVICE_MESSAGEBUILDER_H

#include <stdlib.h>
#include <stdio.h>

//reads in file to string, allocates memory on the heap
char *fileToString(const char *fname);

#endif //QUBYSERVICE_MESSAGEBUILDER_H
