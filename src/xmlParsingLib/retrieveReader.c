//
// Created by elenappli on 11/12/16.
//

#include "retrieveReader.h"

/*Reads a retrieve subset request
Creates a new subStatus ezxml_t to respond to the request
 loops over the retrieve request to fill in the subStatus
 returns a new ezxml_t so its memory needs to be freed after use
 */
ezxml_t readRetrieveRequest(char *msg, size_t l, ezxml_t *status) {
    ezxml_t subStatus = ezxml_new("status");
    ezxml_t rq = ezxml_parse_str(msg, l);

    //iterate over the rq
    //find the requested key name in status xml
    //add it to the subStatus
    ezxml_t it, child;
    for (it = rq->child; it != NULL; it = it->next) {
        //TODO check if it-> exist in status
        child = ezxml_add_child(subStatus, it->txt, 1);
        ezxml_set_txt(child, ezxml_child(*status, it->txt)->txt);
    }

    //clean up
    ezxml_free(rq);
    return subStatus;//needs to be freed after use
}
