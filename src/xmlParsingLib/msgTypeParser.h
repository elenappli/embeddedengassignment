//
// Created by elenappli on 11/15/16.
//

#ifndef QUBYSERVICE_MSGTYPEPARSER_H
#define QUBYSERVICE_MSGTYPEPARSER_H

#include <string.h>

typedef enum {
    UPDATE, RETRIEVE_ALL, RETRIEVE_SUB, INVALID
} msgType;

msgType getMsgType(char *msg);

#endif //QUBYSERVICE_MSGTYPEPARSER_H
