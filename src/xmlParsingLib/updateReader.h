//
// Created by elenappli on 11/12/16.
//

#ifndef QUBYSERVICE_UPDATEREADER_H
#define QUBYSERVICE_UPDATEREADER_H

#include <xmlParsingLib/ezxml/ezxml.h>

#include <string.h>

int readUpdate(char *msg, size_t l, ezxml_t *status);

#endif //QUBYSERVICE_UPDATEREADER_H
