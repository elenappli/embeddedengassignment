//
// Created by elenappli on 11/12/16.
//

#ifndef QUBYSERVICE_RETRIEVEREADER_H
#define QUBYSERVICE_RETRIEVEREADER_H

#include <xmlParsingLib/ezxml/ezxml.h>

ezxml_t readRetrieveRequest(char *msg, size_t l, ezxml_t *status);

#endif //QUBYSERVICE_RETRIEVEREADER_H
