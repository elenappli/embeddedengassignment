//
// Created by elenappli on 11/12/16.
//

#ifndef QUBYSERVICE_STATUSWRITER_H
#define QUBYSERVICE_STATUSWRITER_H

#include <xmlParsingLib/ezxml/ezxml.h>

ezxml_t createNewStatusXml(void);

int writeStatusToFile(ezxml_t *status);

#endif //QUBYSERVICE_STATUSWRITER_H
