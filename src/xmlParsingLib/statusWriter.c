//
// Created by elenappli on 11/12/16.
//

#include "statusWriter.h"

//creates ezxml_t status the heap
ezxml_t createNewStatusXml(void) {
    ezxml_t status = ezxml_new("status");
    return status;
}

/*Assures the status is written to the file lastStatus.xml
 * in a human readable way in the current directory
 * returns 0 on Success, -1 if the file can't be opened
 */
int writeStatusToFile(ezxml_t *status) {
    int ret = 0;//Success
    FILE *fp;
    fp = fopen("lastStatus.xml", "w");
    if (fp == NULL) {
        return -1;//error opening file
    }

    ezxml_t it;
    fprintf(fp, "<%s>\n", "status");
    for (it = (*status)->child; it != NULL; it = it->sibling) {
        fprintf(fp, "    <%s>", it->name);
        fprintf(fp, "%s", it->txt);
        fprintf(fp, "</%s>\n", it->name);
    }
    fprintf(fp, "</%s>\n", "status");
    fclose(fp);
    return ret;
}
