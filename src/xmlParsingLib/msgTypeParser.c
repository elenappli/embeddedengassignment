//
// Created by elenappli on 11/15/16.
//

#include "msgTypeParser.h"

msgType getMsgType(char *msg) {

    msgType type;
    const char *updateMsg = "<update>";
    const char *retrieveAllMsg = "<retrieve/>";
    const char *retrieveSubMsg = "<retrieve>";

    if ((strncmp(msg, updateMsg, 8)) == 0) {
        type = UPDATE;
    } else if (strncmp(msg, retrieveAllMsg, 10) == 0) {
        type = RETRIEVE_ALL;
    } else if (strncmp(msg, retrieveSubMsg, 10) == 0) {
        type = RETRIEVE_SUB;
    } else {
        type = INVALID;
    }

    return type;
}
