//
// Created by elenappli on 11/12/16.
//

#include "updateReader.h"

int readUpdate(char *msg, size_t l, ezxml_t *status) {
    int ret = 0;//Success
    char *msg_copy = malloc(l);
    strcpy(msg_copy, msg);/*remember to copy so the buffer doesn't get the extra
                            char that the ezxml lib adds*/
    ezxml_t update = ezxml_parse_str(msg_copy, l);
    //iterate over update xml
    ezxml_t it, child;
    for (it = update->child; it != NULL; it = it->sibling) {
        child = ezxml_child(*status, it->name);
        if (child != NULL) {//tag exists updates its value
            ezxml_set_txt(child, it->txt);
        } else {//new tag add it and its value
            child = ezxml_add_child(*status, it->name, 1);
            ezxml_set_txt(child, it->txt);
        }
    }

    //clean up
    ezxml_free(update);
    return ret;
}