//
// Created by elenappli on 11/12/16.
//

#ifndef QUBYSERVICE_FRAME_H
#define QUBYSERVICE_FRAME_H

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <xmlParsingLib/ezxml/ezxml.h>

typedef enum {
    HEADER, PAYLOAD
} frameState;

typedef struct {
    uint16_t length; //first 2 bytes is number of bytes in payload
    char *payload; //xml msg
    int index;
    frameState state;
} frame_t;

int addBytesToFrame(frame_t *pFrame, char *pBytes, int numNewBytes);

int buildFrameFromXml(ezxml_t *update, char *buffer);


#endif //QUBYSERVICE_FRAME_H
