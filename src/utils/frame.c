//
// Created by elenappli on 11/12/16.
//

#include <stdio.h>
#include "frame.h"

int addBytesToFrame(frame_t *pFrame, char *pBytes, int numNewBytes) {

    int ii = 0;
    for (ii = 0; ii < numNewBytes; ii++) {
        switch (pFrame->state) {
            case HEADER:
                if (pFrame->index == 0) { //MSByte of length
                    pFrame->length = 0;
                    pFrame->length |= (0xFF00 & (pBytes[ii] << 8));
                    pFrame->index++;
                } else { //LSByte of length
                    pFrame->length |= (0x00FF & (pBytes[ii]));
                    pFrame->index = 0;
                    pFrame->state = PAYLOAD;
                    pFrame->payload = malloc(sizeof(char) * pFrame->length);
                }
                break;
            case PAYLOAD:
                if (pFrame->index < (pFrame->length) - 1) {
                    pFrame->payload[(pFrame->index)++] = pBytes[ii];

                } else {
                    pFrame->payload[(pFrame->index)] = pBytes[ii];
                    pFrame->index = 0;
                    pFrame->state = HEADER;
                    return 1; //got a whole frame_t
                }
                break;
            default:
                pFrame->state = HEADER;
                break;
        }
    }
    return 0;
}

int buildFrameFromXml(ezxml_t *update, char *buffer) {
    char *file;
    file = ezxml_toxml(*update);
    int len = (int) strlen(file);
    int numBytes = len + 2;
    buffer = malloc(sizeof(char) * numBytes);
    //add length to msg
    buffer[0] = (char) (len >> 8);
    buffer[1] = (char) (0x00FF & len);
    int ii;
    for (ii = 0; ii < numBytes - 2; ii++) {
        buffer[2 + ii] = file[ii];
    }
    return numBytes;
}