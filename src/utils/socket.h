//
// Created by elenappli on 11/11/16.
//

#ifndef QUBYSERVICE_SOCKET_H
#define QUBYSERVICE_SOCKET_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros

/* Need to set a max number of clients for select call
 * Can be changed here with changing anything else
 * I just picked 30 as a round number that my system
 * has enough memory to handle
 */
#define MAXCLIENTS 30
#define PORTNUM 6423

typedef struct {
    int serverFd;
    int clientFds[MAXCLIENTS];
    int maxClients;
    struct sockaddr_in serverAddress;
    int serverAddressLength;
} ServerSocket;

void error(const char *msg);

ServerSocket serverInit(void);

int clientInit(void);

void closeServerSocket(ServerSocket *s);

#endif //QUBYSERVICE_SOCKET_H
