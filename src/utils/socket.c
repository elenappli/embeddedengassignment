//
// Created by elenappli on 11/11/16.
//

#include "socket.h"

//called when a system call fails
void error(const char *msg) {
    perror(msg);
    exit(1);
}

/* Opens up the server socket for TCP
 * clears the client socket array
 * so it is ready to accept new client connections
 */
ServerSocket serverInit(void) {
    ServerSocket serverSocket;

    serverSocket.maxClients = MAXCLIENTS;
    //initialize client sockets to 0, none connected yet
    int ii;
    for (ii = 0; ii < serverSocket.maxClients; ii++) {
        serverSocket.clientFds[ii] = 0;
    }

    //create master socket
    //AF_INET - address domain = internet
    //SOCK_STREAM - setting type of socket appropriate for TCP
    //0 - specifies protocol TCP
    serverSocket.serverFd = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket.serverFd < 0) {
        error("ERROR opening socket");
    }

    //set all values in buffer to 0
    bzero((char *) &serverSocket.serverAddress, sizeof(serverSocket.serverAddress));

    serverSocket.serverAddress.sin_family = AF_INET;
    serverSocket.serverAddress.sin_addr.s_addr = INADDR_ANY;//computer IP address
    serverSocket.serverAddress.sin_port = htons(PORTNUM);//convert to network byte order

    //bind socket to address
    if (bind(serverSocket.serverFd, (struct sockaddr *) &serverSocket.serverAddress,
             sizeof(serverSocket.serverAddress)) < 0) {
        error("ERROR on binding");
    }

    //listen for connections, backlog queue set to max
    //5 is the for most systems
    if (listen(serverSocket.serverFd, 5) < 0) {
        error("ERROR on listen");
    }

    serverSocket.serverAddressLength = sizeof(serverSocket.serverAddress);
    return serverSocket;
}

/* Opens up the client socket for TCP
 * connects to server
 * returns the file descriptor of the socket on success
 * else less than 0 if socket couldn't be created or
 * couldn't connect to server
 */
int clientInit(void) {
    int fd;
    struct sockaddr_in serv_addr;

    //AF_INET - address domain = internet
    //SOCK_STREAM - setting type of socket appropriate for TCP
    //0 - specifies protocol TCP
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        error("ERROR opening socket");
    }

    //set all values in buffer to 0
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");//Address of server, localhost for testing
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORTNUM);//convert to network byte order

    if (connect(fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR connecting");

    return fd;
}

/* routine for closing a server socket
 * closes server socket and any connected
 * client sockets
 */
void closeServerSocket(ServerSocket *s) {
    int ii;
    for (ii = 0; ii < s->maxClients; ii++) {
        if (s->clientFds[ii] != 0) {
            close(s->clientFds[ii]);
        }
    }
    close(s->serverFd);
}
