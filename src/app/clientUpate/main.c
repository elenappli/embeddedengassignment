#include <stdio.h>
#include <unistd.h>
#include <utils/socket.h>
#include <testUtils/messageBuilder.h>
#include <utils/frame.h>
#include <xmlParsingLib/ezxml/ezxml.h>

void readSensorsUpdateStatus(ezxml_t *status);

void goToDeepSleep(void);


int main(void) {

    printf("I am the client sending updates to the server\n");
    //open socket
    int fd = clientInit();

    //For testing reading update from file
    ezxml_t update = ezxml_parse_file(
            "/home/elenappli/ClionProjects/QubyService/tests/xmlParsingLib_tests/testXmls/update1.xml");

    //For testing only need to build the msg once
    char *msg, *file;
    file = ezxml_toxml(update);
    int len = (int) strlen(file);
    int numBytes = len + 2;
    msg = malloc(sizeof(char) * numBytes);
    msg[0] = (char) (len >> 8);
    msg[1] = (char) (0x00FF & len);
    int ii;
    for (ii = 0; ii < numBytes - 2; ii++) {
        msg[2 + ii] = file[ii];
    }

    while (1) {
        //get updated sensor data
        readSensorsUpdateStatus(&update);
        //send update
        printf("sending update...\n%s\n\n", ezxml_toxml(update));
        if (send(fd, msg, numBytes, 0) < 0) {
            puts("Send failed");
        }
        //sleep for 15 minutes
        goToDeepSleep();
    }

    ezxml_free(update);
    return 0;
}

void readSensorsUpdateStatus(ezxml_t *status) {
    //empty functions to be filled in with logic
    //to read actual sensor data over serial bus
    //or some other way
}

void goToDeepSleep(void) {
    //Just for testing Demo
    sleep(5);
    //For the actual ARM M0+
    //should call
    //_WFE();
}