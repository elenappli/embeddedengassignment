//
// Created by elenappli on 11/11/16.
//

#include <utils/socket.h>
#include <utils/frame.h>
#include <xmlParsingLib/msgTypeParser.h>
#include <xmlParsingLib/retrieveReader.h>
#include <xmlParsingLib/statusWriter.h>
#include <xmlParsingLib/updateReader.h>
#include <xmlParsingLib/ezxml/ezxml.h>

#include <stdio.h>

int fileExists(const char *filename);

int main() {

    ezxml_t status;
    //check if there exist a file
    FILE *pf;
    if (fileExists("lastStatus.xml") == 1) {
        pf = fopen("lastStatus.xml", "r");
        printf("Reading in last status\n");
        status = ezxml_parse_fp(pf);
        fclose(pf);
    } else {
        printf("No last status, initializing status\n");
        status = createNewStatusXml();
    }


    printf("Current Status\n%s\n", ezxml_toxml(status));

    ServerSocket sock = serverInit();

    //set of socket descriptors
    fd_set readfds;
    int max_sd;
    int socketFd;
    int activity;
    int new_socket, numBytes;
    char buffer[1025];  //data buffer of 1K
    int ii, jj;
    ezxml_t substatus;
    char *response, *msg;
    int respLen = 0;
    //Initialize frame for reading messages
    frame_t cFrame;
    cFrame.length = 0;
    cFrame.index = 0;
    cFrame.state = HEADER;

    printf("starting\n");

    while (1) {
        //clear the socket set
        FD_ZERO(&readfds);

        //add master socket to set
        FD_SET(sock.serverFd, &readfds);
        max_sd = sock.serverFd;

        //add child sockets to set
        for (ii = 0; ii < sock.maxClients; ii++) {
            //socket descriptor
            socketFd = sock.clientFds[ii];

            //if valid socket descriptor then add to read list
            if (socketFd > 0) {
                FD_SET(socketFd, &readfds);
            }
            //highest file descriptor number, need it for the select function
            if (socketFd > max_sd) {
                max_sd = socketFd;
            }
        }

        //wait for an activity on one of the sockets , timeout is NULL ,
        // so wait indefinitely
        printf("waiting for activity...\n");
        activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
        if ((activity < 0) && (errno != EINTR)) {
            printf("select error\n");//log error and move on, select will be called on next iteration
        }

        //It is a new connection
        if (FD_ISSET(sock.serverFd, &readfds)) {
            if ((new_socket = accept(sock.serverFd, (struct sockaddr *) &sock.serverAddress, \
             (socklen_t *) &sock.serverAddressLength)) < 0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            printf("accepted a new connection\n");
            //add new socket to array of sockets
            for (ii = 0; ii < sock.maxClients; ii++) {
                //if position is empty
                if (sock.clientFds[ii] == 0) {
                    sock.clientFds[ii] = new_socket;
                    break;
                }
            }
        }
        // figure out which sockets have pending data
        for (ii = 0; ii < sock.maxClients; ii++) {
            socketFd = sock.clientFds[ii];
            if (!FD_ISSET(socketFd, &readfds)) {
                continue;
            }
            //Check if it was for closing , and also read the incoming message
            numBytes = (int) read(socketFd, buffer, 1024);
            if (0 == numBytes) {
                //Somebody disconnected
                //Close the socket and mark as 0 in list for reuse
                close(socketFd);
                sock.clientFds[ii] = 0;
            } else {
                //Calling read until I get a whole msg
                while ((addBytesToFrame(&cFrame, buffer, numBytes)) == 0) {
                    numBytes = (int) read(socketFd, buffer, 1024);
                }

                //check what kind of msg it is
                msgType type;
                type = getMsgType(cFrame.payload);
                switch (type) {
                    case UPDATE:
                        printf("\nGot update\n");
                        //read report,update status
                        readUpdate(cFrame.payload, strlen(cFrame.payload), &status);
                        printf("Updated status to \n%s\n", ezxml_toxml(status));
                        //write out xml document
                        printf("writing to file\n\n");
                        writeStatusToFile(&status);
                        break;
                    case RETRIEVE_ALL:
                        printf("\nGot retrieve all request\n");

                        msg = ezxml_toxml(status);
                        respLen = strlen(msg);
                        response = malloc(sizeof(char) * (respLen));
                        response[0] = (char) ((respLen) >> 8);
                        response[1] = (char) (0x00FF & (respLen));
                        for (jj = 0; jj < respLen; jj++) {
                            response[2 + jj] = msg[jj];
                        }
                        printf("\nsending response...\n");
                        if (send(socketFd, response, (respLen + 2), 0) < 0) {
                            printf("Send Failed");
                        }
                        free(response);//clean up
                        free(msg);
                        break;
                    case RETRIEVE_SUB:
                        printf("\nGot retrieve some request\n");
                        substatus = readRetrieveRequest(cFrame.payload, strlen(cFrame.payload), &status);
                        msg = ezxml_toxml(substatus);
                        respLen = (int) strlen(msg);
                        response = malloc(sizeof(char) * (respLen));
                        response[0] = (char) ((respLen) >> 8);
                        response[1] = (char) (0x00FF & (respLen));
                        for (jj = 0; jj < respLen; jj++) {
                            response[2 + jj] = msg[jj];
                        }
                        printf("\nsending response...\n");
                        if (send(socketFd, response, (respLen + 2), 0) < 0) {
                            printf("Send Failed");
                        }
                        ezxml_free(substatus);
                        free(response);//clean up
                        free(msg);
                        break;
                    case INVALID:
                        //Close the socket and mark as 0 in list for reuse
                        printf("\nGot invalid Msg\n");
                        close(socketFd);
                        sock.clientFds[ii] = 0;
                        break;
                }
                free(cFrame.payload);
            }
        }

    }

    closeServerSocket(&sock);
    //free memory
    ezxml_free(status);
    return 0;
}


int fileExists(const char *filename) {
    FILE *fp = fopen(filename, "r");
    if (fp != NULL) fclose(fp);
    return (fp != NULL);
}