#include <stdio.h>
#include <unistd.h>
#include <utils/socket.h>
#include <utils/frame.h>
#include <testUtils/messageBuilder.h>
#include <xmlParsingLib/ezxml/ezxml.h>

void printPrettyToScreen(ezxml_t *rsp);

int main(void) {

    printf("I am the client sending retrieve status requests and reading response\n");
    //open socket
    int fd = clientInit();

    //For testing reading request from file
    ezxml_t retrieve = ezxml_parse_file(
            "/home/elenappli/ClionProjects/QubyService/tests/xmlParsingLib_tests/testXmls/retrieve1.xml");

    //For testing only need to build the msg once
    char *msg;
    char *file = ezxml_toxml(retrieve);
    printf("%s\n", file);
    int len = (int) strlen(file);
    int numBytes = len + 2;
    msg = malloc(sizeof(char) * numBytes);
    msg[0] = (char) (len >> 8);
    msg[1] = (char) (0x00FF & len);
    int ii;
    for (ii = 0; ii < numBytes - 2; ii++) {
        msg[2 + ii] = file[ii];
    }
    printf("\n");
    int numBytesRead = 0;
    //Initialize Framer for reading messages
    frame_t frame;
    frame.length = 0;
    frame.index = 0;
    frame.state = HEADER;
    char buffer[1025];  //data buffer of 1K
    ezxml_t response;

    while (1) {
        buildFrameFromXml(&retrieve, msg);
        //send update
        printf("sending msg...\n%s\n", file);
        if (send(fd, msg, numBytes, 0) < 0) {
            puts("Send failed");
        }

        //Read response back
        do {
            numBytesRead = (int) read(fd, buffer, 1024);
        } while (addBytesToFrame(&frame, buffer, numBytesRead) == 0);
        printf("Read response...\n");

        response = ezxml_parse_str(frame.payload, strlen(frame.payload));
        printPrettyToScreen(&response);
        free(frame.payload);


        sleep(10);
    }

    return 0;
}

void printPrettyToScreen(ezxml_t *rsp) {
    ezxml_t it;
    printf("<%s>\n", "status");
    for (it = (*rsp)->child; it != NULL; it = it->sibling) {
        printf("    <%s>", it->name);
        printf("%s", it->txt);
        printf("</%s>\n", it->name);
    }
    printf("</%s>\n\n", "status");
}